<?php
require 'init.php';
eatit_header();
$rid = get_rid_from_cart(session_uid());
?>

<h2> <?php echo eatit_get_title();?> </h2>

<div class="box">
<?php eatit_get_cart();?>
</div>

<form class="order-form" action="" method="post">
  <div class="box">
    <h3>Address</h3>
    <select class="address-select" name="address">
        <option value="0" selected>--select address--</option>
        <?php array_map(function ($d) {echo "<option value='".$d['aid']."'>".eatit_stringify_address(get_address_by_id($d['aid']))."</option>";},get_user_restaurant_intersect(session_uid(),$rid)); ?>
    </select>
  </div>

    <div class="box">

    <h3>Payment Type</h3>
    <select class="payment-select" name="payment">
        <option  selected>--select payment type--</option>
        <?php array_map(function ($d) {echo "<option value='".$d."'>".$d."</option>";},get_payment_types_by_restaurant($rid));?>
    </select>
  </div>
<div class="box">

    <h3> Delivery Time </h3>
    <input type="radio" name="delivery_time" value="Now"> Now<br>
    <input type="radio" name="delivery_time" value="Later"> Later
    <input type="text" name="exact_delivery_time" value="2016-05-16 00:00:00">
    <br>
  </div>

    <div class="box">

    <h3>Additional Notes</h3>
    <textarea name="additional_notes" placeholder="Extra..."></textarea>
<br></br>
</div>
    <input name='finish_order_button' type='submit' value='Submit Cart'>
</form>
<br> </br>

<?php if(isset($_POST["finish_order_button"])){

    $cart = get_items_on_cart(session_uid());
    $diff = number_format(array_reduce(array_map(function ($i) {return $i['count']*$i['price'];},$cart), function ($a,$b) {return $a+$b;}, 0),2)
       >  get_minimum_cost_for_district($rid,get_address_by_id($_POST["address"])["district_code"]);
    if($diff){
        if(strcmp($_POST["delivery_time"],"Now") == 0){
            if(create_order(session_uid(),date("Y-m-d H:i:s"),$_POST["additional_notes"],$_POST["payment"],$_POST["address"],$rid))
                redirect("index.php");
        } else {
            if(strtotime($_POST["exact_delivery_time"]) - strtotime(date("Y-m-d H:i:s")) < 172800){
                if(create_order(session_uid(),$_POST["exact_delivery_time"],$_POST["additional_notes"],$_POST["payment"],$_POST["address"],$rid))
                    redirect("index.php");
                else
                    echo "Fail";
                } else {
                    echo "WARNING: Your submission date must be at most two days!";
                    echo "<br> </br>";
                    echo "<br> </br>";
            }
        }
    } else {
        echo "WARNING: Your total cart is lower than the expected order price of the restaurant!";
        echo "<br> </br>";
        echo "<br> </br>";
    }
}

eatit_footer();
