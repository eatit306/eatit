<?php
require 'init.php';
eatit_header();

echo eatit_get_title();
?>
<?php if (is_user_logged_in()): ?>
<h2>Write Your Feedback</h2>
<div class="feedback">
  <?php eatit_feedback_list(); ?>
</div>
<?php endif;
eatit_footer();
