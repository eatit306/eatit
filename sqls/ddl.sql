DROP DATABASE IF EXISTS eatit;
CREATE DATABASE eatit;
USE eatit;

/* ENTITIES */

CREATE TABLE cities
(
    city_code    integer(11) NOT NULL,
    city_name    varchar(255) NOT NULL,

    PRIMARY KEY (city_code)
);

CREATE TABLE districts
(
    district_code    integer(11) NOT NULL AUTO_INCREMENT,
    district_name    varchar(255) NOT NULL,
    city_code    integer(11) NOT NULL,

    PRIMARY KEY (district_code),
    FOREIGN KEY (city_code) REFERENCES cities(city_code) ON DELETE CASCADE
);

CREATE TABLE item_types
(
    item_type    varchar(128),

    PRIMARY KEY (item_type)
);

CREATE TABLE payment_types
(
    payment_type    varchar(128),

    PRIMARY KEY (payment_type)
);

CREATE TABLE user
(
    uid        integer(10) NOT NULL AUTO_INCREMENT,
    username        varchar(32)    NOT NULL UNIQUE,
    password        varchar(64)    NOT NULL,
    name        varchar(32)    NOT NULL,
    surname        varchar(32)    NOT NULL,
    phone        varchar(16)    NOT NULL,

    PRIMARY KEY (uid)
);

CREATE TABLE admin
(
    uid        integer(10),

    PRIMARY KEY (uid),
    FOREIGN KEY (uid) REFERENCES user(uid) ON DELETE CASCADE
);

CREATE TABLE customer
(
    uid        integer(10),
    bonus        integer(10)    NOT NULL,

    PRIMARY KEY (uid),
    FOREIGN KEY (uid) REFERENCES user(uid) ON DELETE CASCADE
);

CREATE TABLE frequent_customer
(
    uid        integer(10),

    PRIMARY KEY (uid),
    FOREIGN KEY (uid) REFERENCES customer(uid) ON DELETE CASCADE
);

CREATE TABLE owner
(
    uid        integer(10),

    PRIMARY KEY (uid),
    FOREIGN KEY (uid) REFERENCES user(uid) ON DELETE CASCADE
);

CREATE TABLE address
(
    aid        integer(11) NOT NULL AUTO_INCREMENT,
    door_number    integer(11),
    street        varchar(255),
district_code    integer(11),
    uid        integer(10)    NOT NULL,

    PRIMARY KEY (aid),
    FOREIGN KEY (district_code) REFERENCES districts(district_code) ON DELETE SET NULL,
    FOREIGN KEY (uid) REFERENCES user(uid) ON DELETE CASCADE
);

CREATE TABLE restaurant
(
    rid        integer(11) NOT NULL AUTO_INCREMENT,
    name        varchar(32)    NOT NULL,
    open_hour    time        NOT NULL,
    close_hour    time        NOT NULL,
    is_open        boolean,
    rank        numeric(2,1),
    aid        integer(11)    NOT NULL,

    PRIMARY KEY (rid),
    FOREIGN KEY (aid) REFERENCES address(aid) ON DELETE CASCADE
);

CREATE TABLE menu_item
(
    iid        integer(10) NOT NULL AUTO_INCREMENT,
    name        varchar(64)    NOT NULL,
    price        numeric(10,2)    NOT NULL,
    is_available    boolean    NOT NULL,
    rank        numeric(2,1),
    item_type    varchar(128),

    PRIMARY KEY (iid),
    FOREIGN KEY (item_type) REFERENCES item_types(item_type) ON DELETE SET NULL
);

CREATE TABLE orders
(
    oid        integer(11) NOT NULL AUTO_INCREMENT,
    total_cost    numeric(12,2)    NOT NULL,
    order_date    timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    delivery_date    timestamp    NOT NULL,
    notes        varchar(255),
    payment_type    varchar(128),
    is_delivered  boolean DEFAULT 0,
    rid        integer(11)    NOT NULL,
    aid        integer(11)    NOT NULL,

    PRIMARY KEY (oid),
FOREIGN KEY (payment_type) REFERENCES payment_types(payment_type) ON DELETE SET NULL,
FOREIGN KEY (rid) REFERENCES restaurant(rid) ON DELETE CASCADE,
FOREIGN KEY (aid) REFERENCES address(aid) ON DELETE CASCADE
);

CREATE TABLE order_feedback
(
    oid        integer(11),
    ranking        numeric(2,1)    NOT NULL,
    comment        varchar(255),

    PRIMARY KEY (oid),
FOREIGN KEY (oid) REFERENCES orders(oid) ON DELETE CASCADE
);

CREATE TABLE item_feedback
(
    oid        integer(11),
    iid        integer(11),
    ranking        numeric(2,1)    NOT NULL,
    comment        varchar(255),

    PRIMARY KEY (oid,iid),
FOREIGN KEY (oid) REFERENCES orders(oid) ON DELETE CASCADE,
FOREIGN KEY (iid) REFERENCES menu_item(iid) ON DELETE CASCADE

);

/* RELATIONS */

CREATE TABLE serves_district
(
    rid        integer(11),
    district_code    integer(11),
    delivery_time    integer(3)    NOT NULL,
    min_cost        numeric(10,2)    NOT NULL,

    PRIMARY KEY (rid,district_code),
    FOREIGN KEY (rid) REFERENCES restaurant(rid) ON DELETE CASCADE,
    FOREIGN KEY (district_code) REFERENCES districts(district_code) ON DELETE CASCADE
);

CREATE TABLE can_pay
(
    rid        integer(11),
    payment_type    varchar(128),

    PRIMARY KEY (rid,payment_type),
    FOREIGN KEY (rid) REFERENCES restaurant(rid) ON DELETE CASCADE,
    FOREIGN KEY (payment_type) REFERENCES payment_types(payment_type) ON DELETE CASCADE
);

CREATE TABLE promotes
(
    pid        integer(10),
    iid        integer(10),
    number        integer(4),

    PRIMARY KEY (pid,iid),
    FOREIGN KEY (pid) REFERENCES menu_item(iid) ON DELETE CASCADE,
    FOREIGN KEY (iid) REFERENCES menu_item(iid) ON DELETE CASCADE
);

CREATE TABLE cart
(
    uid        integer(10),
    iid        integer(10),
    count        integer(4),

    PRIMARY KEY (uid,iid),
    FOREIGN KEY (uid) REFERENCES customer(uid) ON DELETE CASCADE,
    FOREIGN KEY (iid) REFERENCES menu_item(iid) ON DELETE CASCADE
);

CREATE TABLE menu
(
    rid        integer(11),
    iid        integer(10),

    PRIMARY KEY (rid,iid),
    FOREIGN KEY (rid) REFERENCES restaurant(rid) ON DELETE CASCADE,
    FOREIGN KEY (iid) REFERENCES menu_item(iid) ON DELETE CASCADE
);

CREATE TABLE belongs
(
  oid        integer(11),
  iid        integer(10),
  count        integer(4),

  PRIMARY KEY (oid,iid),
  FOREIGN KEY (oid) REFERENCES orders(oid) ON DELETE CASCADE,
  FOREIGN KEY (iid) REFERENCES menu_item(iid) ON DELETE CASCADE
);
