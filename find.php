<?php
require 'init.php';
eatit_header();
?>

<?php
$response = eatit_find_get_param();
?>
<h2>Restaurants serving <?php echo $response["district_name"]; ?></h2>
<div class="restaurants">
  <?php eatit_restaurant_list_for_district($response["district_code"]) ?>
</div>

<?php
eatit_footer();
