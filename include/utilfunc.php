<?php
function array_map_assoc($func,$arr) {
  $y = array();
  foreach ($arr as $key => $val)
    array_push($y,$func($key,$val));
  return $y;
}

function check_time($t1, $t2, $tn) {
  $t1 = +str_replace(":", "", $t1);
   $t2 = +str_replace(":", "", $t2);
   $tn = +str_replace(":", "", $tn);
    if ($t2 >= $t1) {
       return $t1 <= $tn && $tn < $t2;
   } else {
       return ! ($t2 <= $tn && $tn < $t1);
   }
}

function redirect($url){
    if (headers_sent()){
      die('<script type="text/javascript">window.location=\''.$url.'\';</script>');
    }else{
      header('Location: ' . $url);
      die();
    }
}
