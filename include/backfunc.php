<?php
include 'utilfunc.php';
include 'dbfunc.php';

function the_user() {
  return session_uid() > 0 ? get_user_by("uid", session_uid()) : array();
}

function get_user_by($by, $val) {
  if (! in_array($by, array("uid","username"))) return false;
  $val = $by == "uid" ? $val : "'$val'";
  $ans = db_select(array("user"),array("uid","username","name","surname","phone"),array("$by = $val"));
  return $ans["error"] ? false : ($ans['count'] > 0 ? $ans["rows"][0] : array());
}

function is_correct_password($uid, $password) {
  $ans = db_select(array("user"),array("password"),array("uid = $uid"));
  return $ans["rows"][0]["password"] == $password;
}

function create_customer($username, $password, $name, $surname, $phone) {
  $uid = insert_new_user($username, $password, $name, $surname, $phone);
  if ($uid) set_user_role($uid, "customer");
  return $uid;
}

function is_customer($uid) {
  return db_select(array("customer"),array("uid"),array("uid = $uid"))['count'] > 0;
}

function create_owner($username, $password, $name, $surname, $phone) {
  $uid = insert_new_user($username, $password, $name, $surname, $phone);
  if ($uid) set_user_role($uid, "owner");
  return $uid;
}

function is_owner($uid) {
  return db_select(array("owner"),array("uid"),array("uid = $uid"))['count'] > 0;
}

function create_admin($username, $password, $name, $surname, $phone) {
  $uid = insert_new_user($username, $password, $name, $surname, $phone);
  if ($uid) set_user_role($uid, "admin");
  return $uid;
}

function is_admin($uid) {
  return db_select(array("admin"),array("uid"),array("uid = $uid"))['count'] > 0;
}

function insert_new_user($username, $password, $name, $surname, $phone) {
  $ans = db_select(array("user"),array("uid"),array("username = '$username'"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0) return false;
  $ans = db_insert("user",array($username,$password,$name,$surname,$phone),array("username","password","name","surname","phone"));
  return ($ans['count']>0) ? get_user_by("username",$username)['uid'] : false;
}

function update_user($uid, $changes) {
  $ans = db_select(array("user"),array("uid"),array("uid = $uid"));
  if ($ans['error']) return false;
  if ($ans['count'] == 0) return false;
  $ans = db_update("user",$changes,array("uid = $uid"));
  return $ans['count'] > 0;
}

function set_user_role($uid, $role) {
  if (! in_array($role, array("customer","owner", "admin"))) return false;
  $ans = db_insert($role,array($uid),array("uid"));
  return ($ans['count']>0) ? true : false;
}

function remove_user($uid) {
  // return true if successful else false
  $ans = db_select(array("user"),array("uid"),array("uid = '$uid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_delete("user",array("uid = '$uid'"));
  return ($ans['count']>0) ? true : false;
}

function get_address_by_id($aid) {
  $ans = db_select(array("address"),array("aid","door_number","street","district_code","uid"),array("aid = $aid"));
  if ($ans["error"]) {
    return false;
  } else if ($ans['count'] == 0) {
    return array();
  } else {
    $address = $ans["rows"][0];
    $ans = db_select(array("districts"),array("district_name","city_code"),array("district_code = ".$address['district_code']))["rows"][0];
    $address["district_name"] = $ans["district_name"];
    $address["city_name"] = db_select(array("cities"),array("city_name"),array("city_code = ".$ans['city_code']))["rows"][0]["city_name"];
    return $address;
  }
}

function create_address($uid,$door_number,$street,$district_code) {
  // insert new address and connect to the user. return aid
  $ans = db_select(array("user"),array("uid"),array("uid = '$uid'"));
  if ($ans['error']) return false;
  $ans = db_insert("address",array($door_number,$street,$district_code,$uid),array("door_number","street","district_code","uid"));
  if ($ans['error']) return false;
  $ans = db_select(array("address"),array("aid"),array("uid = '$uid'","door_number = '$door_number'","street = '$street'", "district_code = '$district_code'"));
  return ($ans['count']>0) ? max(array_map(function($e){return $e['aid'];},$ans['rows'])) : false;
}

function remove_address($aid) {
  // return true if successful else false
  $ans = db_select(array("address"),array("aid"),array("aid = '$aid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_delete("address",array("aid = '$aid'"));
  return ($ans['count']>0) ? true : false;
}

function get_addresses_of_user($uid) {
  $ans = db_select(array("address"),array("aid"),array("uid = $uid"));
  return $ans["error"] ? false : array_map(function($e){return $e['aid'];},$ans['rows']);
}

function get_all_cities() {
  return db_select(array("cities"))["rows"];
}

function get_all_districts() {
  return db_select(array("districts"))["rows"];
}

function get_all_payment_types() {
  return db_select(array("payment_types"))["rows"];
}

function get_all_item_types() {
  return db_select(array("item_types"))["rows"];
}

function get_restaurant_by($by, $val) {
  if (! in_array($by, array("rid","name","aid"))) return false;
  $val = in_array($by, array("rid","aid")) ? $val : "'$val'";
  $ans = db_select(array("restaurant"),array("rid","name","open_hour","close_hour","is_open","rank","aid"),array("$by = $val"));
  return $ans["error"] ? false : ($ans['count'] > 0 ? $ans["rows"][0] : array());
}

function create_restaurant($aid,$name,$open_hour=0,$close_hour=0) {
  // return db_insert("restaurant",array($name,$open_hour,$close_hour,$aid),array("name","open_hour","close_hour","aid"));
  $ans = db_select(array("address"),array("aid"),array("aid = '$aid'"));
  if ($ans['error']) return false;
  $ans = db_insert("restaurant",array($name,$open_hour,$close_hour,$aid),array("name","open_hour","close_hour","aid"));
  $rid = $ans['id'];
  if ($ans['error']) return false;
  $ans = db_select(array("restaurant"),array("rid"),array("name = '$name'","open_hour = '$open_hour'","close_hour = '$close_hour'","aid = '$aid'"));
  return ($ans['count']>0) ? $rid : false;
}

function remove_restaurant($rid) {
  // return true if successful else false
  $ans = db_select(array("restaurant"),array("rid"),array("rid = '$rid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_select(array("orders"),array("is_delivered"),array("rid = '$rid'"));
  $is_delivered = array_map(function($e){return $e['is_delivered'];},$ans['rows']);
  if(in_array(0,$is_delivered)) return false;
  $ans = db_delete("menu_item",array("menu_item.iid IN (SELECT menu.iid from menu where rid = '$rid')"));
  $ans = db_delete("restaurant",array("rid = '$rid'"));
  return ($ans['count']>0) ? true : false;
}

function update_restaurant($rid, $changes) {
  return db_update("restaurant",$changes,array("rid = $rid"));
}

function set_restaurant_open($rid, $open) {
  return update_restaurant($rid, array("is_open" => $open));
}

function is_restaurant_open($rid) {
  // return true if is_open AND (between open_hour and close_hour)
  // time may be okay but restaurant can be closed for a different reason
  $ans = db_select(array("restaurant"),array("is_open"),array("rid = '$rid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  if (!array_map(function($e){return $e['is_open'];},$ans['rows'])[0]) return false;
  $open = db_select(array("restaurant"),array("open_hour"),array("rid = '$rid'"));
  if ($open['error']) return false;
  $close = db_select(array("restaurant"),array("close_hour"),array("rid = '$rid'"));
  if ($close['error']) return false;
  $open = array_map(function($e){return $e['open_hour'];},$open['rows'])[0];
  $close = array_map(function($e){return $e['close_hour'];},$close['rows'])[0];

  return check_time($open,$close,date('H:i:s'));

}
function get_payment_types_by_restaurant($rid) {
  $ans = db_select(array("can_pay"),array("payment_type"),array("rid = $rid"));
  return ($ans['error']) ? false : ($ans['count']>0 ? array_map(function ($p) {return $p["payment_type"];},$ans["rows"]) : array());
}

function add_payment_option($rid,$payment_type) {
  // can_pay table
  $ans = db_select(array("can_pay"),array("rid","payment_type"),array("rid = '$rid'","payment_type = '$payment_type'"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0) return false;
  $ans = db_insert("can_pay",array($rid,$payment_type),array("rid","payment_type"));
  return ($ans['count']>0) ? true : false;
}

function remove_payment_option($rid,$payment_type) {
  // can_pay table
  $ans = db_select(array("can_pay"),array("rid","payment_type"),array("rid = '$rid'","payment_type = '$payment_type'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_delete("can_pay",array("rid = '$rid'","payment_type = '$payment_type'"));
  return ($ans['count']>0) ? true : false;
}

function get_service_districts($rid) {
  $ans = db_select(array("serves_district","districts"),array("*"),array("serves_district.district_code = districts.district_code","rid = $rid"));
  return ($ans['error']) ? false : ($ans['count']>0 ? $ans["rows"] : array());
}

function add_service_district($rid,$district,$deltime,$mincost) {
  // serves_district
  $ans = db_select(array("serves_district"),array("rid","district_code"),array("rid = '$rid'","district_code = '$district'"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0) return false;
  $ans = db_insert("serves_district",array($rid,$district,$deltime,$mincost),array("rid","district_code","delivery_time","min_cost"));
  return ($ans['count']>0) ? true : false;
}

function remove_service_district($rid,$district) {
  // serves_district
  $ans = db_select(array("serves_district"),array("rid","district_code"),array("rid = '$rid'","district_code = '$district'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_delete("serves_district",array("rid = '$rid'","district_code = '$district'"));
  return ($ans['count']>0) ? true : false;
}

function create_menu_item($name, $price, $item_type) {
  $ans = db_insert("menu_item",array($name,$price,$item_type,1),array("name","price","item_type","is_available"));
  return ($ans['count']>0) ? $ans["id"] : false;
}

function remove_menu_item($iid) {
  $ans = db_select(array("menu_item"),array("iid"),array("iid = '$iid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $ans = db_delete("menu_item",array("iid = '$iid'"));
  return ($ans['count']>0) ? true : false;
}

function update_menu_item($iid, $changes) {
  return db_update("menu_item",$changes,array("iid = $iid"));
}

function set_menu_item_available($iid, $available) {
  return update_menu_item($iid,array("is_available" => $available));
}

function is_menu_item_available($iid) {
  $ans = db_select(array("menu_item"),array("is_available"),array("iid = '$iid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $is_available = array_map(function($e){return $e['is_available'];},$ans['rows'])[0];
  return ($is_available) ? true : false;

}

function add_item_to_menu($iid,$rid) {
  $ans = db_select(array("menu"),array("rid","iid"),array("iid = $iid","rid = $rid"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0) return false;
  $ans = db_insert("menu",array($rid,$iid),array("rid","iid"));
  return ($ans['count']>0) ? true : false;
}

function get_items_in_menu($rid) {
  $ans = db_select(array("menu","menu_item"), array("menu_item.iid","name","price","is_available","rank","item_type"), array("rid = $rid","menu.iid = menu_item.iid"));
  return $ans["error"] ? false : $ans['rows'];
}

function get_available_items_in_menu($rid) {
  return array_filter(get_items_in_menu($rid),function($item){return $item['is_available'];});
}

function add_item_to_cart($uid,$iid,$rid,$count=1) {
  // cart table
  $ans = db_select(array("cart"),array("uid","iid","count"),array("uid = '$uid'","iid = '$iid'"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0){
    $count_db = $ans['rows'][0]['count'];
    $count_db += $count;
    $ans = db_update("cart",array("count" => $count_db),array("uid = '$uid'","iid = '$iid'"));
  }
  else{
    $rids = db_select(array("cart LEFT OUTER JOIN menu ON menu.iid = cart.iid"),array("menu.rid"),array("menu.rid <> $rid","cart.uid = $uid"));
    if($rids['count'] > 0) return false;
    $ans = db_insert("cart",array($uid,$iid,$count),array("uid","iid","count"));
  }
  return ($ans['count']>0) ? true : false;
}

function get_rid_from_cart($uid){
  $ans = db_select(array("cart"),array("uid","iid","count"),array("uid = '$uid'"));
  if ($ans['error']) return false;
  $ans = db_select(array("cart LEFT OUTER JOIN menu ON menu.iid = cart.iid"),array("menu.rid"),array("cart.uid = $uid"));
  return ($ans['count']>0) ? $ans['rows'][0]['rid'] : false;
}

function remove_item_from_cart($uid,$iid) {
  $ans = db_delete("cart",array("uid = $uid","iid = $iid"));
  return $ans["error"] ? false : $ans["count"] > 0 ;
}

function get_items_on_cart($uid) {
  $ans = db_select(array("cart","menu_item"), array("menu_item.iid","name","price","is_available","rank","item_type","count"),array("cart.iid = menu_item.iid","uid = $uid"));
  return $ans['error'] ? false : $ans['rows'];
}

function clear_cart($uid){
  $ans = db_delete("cart",array("uid = $uid"));
  return $ans["error"] ? false : $ans["count"] > 0 ;
}

function create_order($uid,$delivery_date,$notes,$payment_type,$aid,$rid) {
  $ans = db_select(array("cart"),array("uid"),array("uid = '$uid'"));
  if ($ans['error']) return false;
  if ($ans['count'] <= 0) return false;
  $cart = get_items_on_cart(session_uid());
  $total_cost = number_format(array_reduce(array_map(function ($i) {return $i['count']*$i['price'];},$cart), function ($a,$b) {return $a+$b;}, 0),2);
  $ans = db_insert("orders",array($delivery_date,$notes,$payment_type,$total_cost,$aid,0,$rid),array("delivery_date","notes","payment_type","total_cost","aid","is_delivered","rid"));
  var_dump($ans);
  if ($ans['error']) return false;
  if ($ans['count']<=0) return false;
  $oid = $ans['id'];
  $ans = db_insert("belongs",array_map(function ($i) use($oid) {return array($oid,$i["iid"],$i["count"]);},$cart), array("oid","iid","count"), true);
  clear_cart($uid);
  return ($ans['count']>0) ? true : false;
}

function get_district_by_code($code) {
  $r = array_values(array_filter(get_all_districts(), function ($d) use ($code) {return $d["district_code"] == $code; }));
  return empty($r) ? array() : $r[0];
}

function get_restaurants_in_district($district) {
  $ans = db_select(array("restaurant LEFT OUTER JOIN serves_district ON restaurant.rid = serves_district.rid"),array("restaurant.rid","name","open_hour","close_hour","is_open","rank","aid","district_code","delivery_time","min_cost"),array("district_code = $district OR district_code IS NULL"),"ORDER BY delivery_time ASC");
  return $ans["error"] ? false : $ans["rows"];
}

function get_recent_orders($uid){
  $ans = db_select(array("orders LEFT OUTER JOIN address ON address.aid = orders.aid LEFT OUTER JOIN belongs ON belongs.oid = orders.oid LEFT OUTER JOIN menu_item ON menu_item.iid = belongs.iid LEFT OUTER JOIN restaurant ON restaurant.rid = orders.rid"),array("orders.*,GROUP_CONCAT(menu_item.name,' x ',belongs.count) as order_items,restaurant.name as restaurant_name, orders.is_delivered"),array("uid = $uid AND DATE(order_date) >= CURDATE() - INTERVAL 14 DAY"),"GROUP BY orders.oid ORDER BY order_date DESC");
  return $ans["error"] ? false : $ans["rows"];
}

function get_order_feedback($oid){
  $ans = db_select(array("order_feedback"),array("oid","ranking","comment"),array("oid = '$oid'"));
  return $ans["error"] ? false : $ans["rows"];
}

function add_order_feedback($oid,$comment,$ranking){
  $ans = db_select(array("order_feedback"),array("oid"),array("oid = '$oid'"));
  if ($ans['error']) return false;
  if ($ans['count'] > 0) return false;
  $ans = db_insert("order_feedback",array($oid,$comment,$ranking),array("oid","comment","ranking"));
  if ($ans['error']) return false;
  $rid = db_select(array("orders"),array("rid"),array("oid = '$oid'"))['rows'][0]['rid'];
  $order_count = db_select(array("order_feedback LEFT OUTER JOIN orders ON orders.oid = order_feedback.oid"),array("COUNT(*)"),array("orders.rid = '$rid'") )['rows'][0]['COUNT(*)'];
  $order_count--;
  $current_rank = db_select(array("restaurant"),array("rank"),array("rid = '$rid'"))['rows'][0]['rank'];
  $current_rank = ($current_rank * $order_count) / ($order_count + 1);
  $current_rank = $current_rank + $ranking / ($order_count + 1);
  $ans = db_update("restaurant",array("rank" => $current_rank),array("rid = '$rid'"));
  return ($ans['count']>0) ? true : false;
}

function get_most_ordered_items($rid) {
  $ans = db_select(array("menu","menu_item"), array("menu_item.iid","name","price","is_available","rank","item_type","(SELECT SUM(belongs.count) from belongs where belongs.iid = menu.iid) as ordered_count"), array("rid = $rid","menu.iid = menu_item.iid"),"ORDER BY ordered_count desc LIMIT 5");
  return $ans["error"] ? false : $ans['rows'];
}

function get_restaurants_of_user($uid) {
  $ans = db_select(array("address","restaurant"), array("rid"), array("address.aid = restaurant.aid", "uid = $uid"));
  return $ans["error"] ? false : $ans['rows'];
}

function get_orders_for_owner($uid) {
  $ans = db_select(array("orders"),array("*"),array(implode(" OR ",array_map(function ($a) {return "rid = ".$a['rid'];}, get_restaurants_of_user($uid) ))),"ORDER BY delivery_date DESC");
  return $ans["error"] ? false : $ans["rows"];
}

function get_items_in_order($oid) {
  $ans = db_select(array("belongs","menu_item"),array("*"),array("belongs.iid = menu_item.iid","oid = $oid"));
  return $ans["error"] ? false : $ans["rows"];
}

function set_order_delivered($oid,$delivered) {
    $ans = db_update("orders",array("is_delivered" => $delivered),array("oid = $oid"));
    return $ans["error"] ? false : $ans['count'] > 0;
}

function get_user_restaurant_intersect($uid,$rid){
  $ans = db_select(array("restaurant LEFT OUTER JOIN address ON address.aid = restaurant.aid LEFT OUTER JOIN address ad ON ad.uid = '$uid' AND ad.district_code = address.district_code"), array("ad.*"), array("restaurant.rid = '$rid'"));
  return $ans["error"] ? false : $ans['rows'];
}

function get_minimum_cost_for_district($rid,$district_code) {
  $ans = db_select(array("serves_district"),array("min_cost"),array("rid = $rid","district_code = $district_code"));
  return $ans["error"] ? false : ($ans['count'] > 0) ? $ans['rows'][0]['min_cost'] : 0;
}

function add_to_promotion($pid, $iid, $number) {
  $ans = db_insert("promotes",array($pid,$iid,$number),array("pid","iid","number"));
  return ($ans['count']>0) ? true : false;
}

function is_promotion($iid) {
  return db_select(array("promotes"),array("pid"),array("pid = $iid"))['count'] > 0;
}

function get_promoted_items($pid) {
  $ans = db_select(array("promotes","menu_item"),array("*"),array("promotes.iid = menu_item.iid","pid = $pid"));
  return $ans["error"] ? false : ($ans['count'] > 0) ? $ans['rows'] : array();
}
