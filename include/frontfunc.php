<?php

function eatit_head() {
  eatit_session_init();
  eatit_sorting_hat();
}

function eatit_foot() {

}

function eatit_header() {
  eatit_head();
  ?>
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title><?php echo eatit_get_title(); ?></title>
    <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link href="css/site.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div id="wrapper">
      <header>
        <a href="index.php"><h1>EatIT: A COMP306 Project!</h1></a>
      </header>
      <div id="container">
        <table id="main-table">
          <tr>
            <td id="sidebar-td">
              <?php eatit_sidebar_content() ?>
            </td>
            <td id="content-td">
              <div id="content">
                <?php
              }

              function eatit_footer() {
                ?>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <footer>
        EatIT: A COMP306 Project, 2016
      </footer>
    </div>
  </body>
  </html>
  <?php
}

function eatit_sidebar_content() {
  ?>
  <div id="sidebar">
    <?php if (is_user_logged_in()) : ?>
      <p>
        Hello <?php echo the_user()["name"]; ?>!
      </p>
      <ul>
        <li><a href="profile.php">Edit profile</a></li>
        <li><a href="logout.php">Log out</a></li>
      </ul>
      <?php if (is_customer(session_uid()) && ! in_array(get_current_page(),array("order"))): ?>
      <div class="cart box">
        <?php eatit_get_cart(); ?>
      </div>
      <form id='move_to_order_menu' method='post' action='order.php'>
            <input name='move_to_order_menu_button' type='submit' value='Order Cart'>
      </form>
    <?php elseif (is_owner(session_uid())) : ?>

    <?php endif; ?>

    <?php else: ?>
      <p>
        Hello stranger!
      </p>
      <?php eatit_login_form(); ?>
      <p>
        Not a member? <a href="register.php">Register now!</a>
      </p>
    <?php endif; ?>
  </div>
  <?php
}

function eatit_get_title() {
  $title = "EatIT - ";
  switch (get_current_page()) {
    case 'index':
    return $title."Homepage";
    case 'register':
    return $title."Register";
    case 'login':
    return $title."Login";
    case 'find':
    return $title."Find Restaurant";
    case 'order':
    return $title."Your order";
    case 'restaurant':
    return $title.get_restaurant_by("rid",$_GET["rid"])["name"];
    default:
    return $title."A COMP306 Project";
  }
}

function eatit_registration_form() {
  $response = eatit_register();
  if (isset($response["success"]) && $response["success"])
  redirect("login.php?uid=".$response['uid']);
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="registration-form a-form" action="register.php" method="post">
  <label><input type="radio" name="role" value="customer" <?php echo isset($response["role"]) ? ($response["role"] == "customer" ? "checked" : "") : "checked"; ?>>Customer</label>
  <label><input type="radio" name="role" value="owner" <?php echo isset($response["role"]) ? ($response["role"] == "owner" ? "checked" : "") : ""; ?>>Restaurant Owner</label>
  <input type="text" name="username" value="<?php echo isset($response["username"]) ? $response["username"] : ""; ?>" placeholder="username">
  <input type="password" name="password" value="" placeholder="password">
  <input type="password" name="password2" value="" placeholder="repeat password">
  <input type="text" name="firstname" value="<?php echo isset($response["firstname"]) ? $response["firstname"] : ""; ?>" placeholder="first name">
  <input type="text" name="lastname" value="<?php echo isset($response["lastname"]) ? $response["lastname"] : ""; ?>" placeholder="last name">
  <input type="text" name="phone" value="<?php echo isset($response["phone"]) ? $response["phone"] : ""; ?>" placeholder="phone number">
  <input type="submit" name="register-submit" value="Register">
</form>
<?php
}

function eatit_register() {
  if (! isset($_POST["register-submit"])) {
    return array("error"=>false);
  } else {
    $raw = array(
      "role" => $_POST["role"],
      "username" => $_POST["username"],
      "password" => $_POST["password"],
      "password2" => $_POST["password2"],
      "firstname" => $_POST["firstname"],
      "lastname" => $_POST["lastname"],
      "phone" => $_POST["phone"],
    );
    $valid = eatit_register_validate($raw);
    if ($valid["error"]) {
      $valid["password"] = "";
      $valid["password2"] = "";
      return $valid;
    } else {
      $ready = eatit_register_sanitize($valid);
      if (! empty(get_user_by("username",$ready["username"]))) {
        $valid["error"] = true;
        $valid["reason"] = array("There already exists a registered user with the same username. Please choose a different username.");
        $valid["password"] = "";
        $valid["password2"] = "";
        return $valid;
      } else {
        // everything up to here looks cool!
        if($ready["role"] == "owner") {
          $uid = create_owner($ready["username"],$ready["password"],$ready["firstname"],$ready["lastname"],$ready["phone"]);
        } else {
          $uid = create_customer($ready["username"],$ready["password"],$ready["firstname"],$ready["lastname"],$ready["phone"]);
        }
        return array("error"=>false,"success"=>true, "uid"=>$uid);
      }
    }
  }
}

function eatit_register_validate($input) {
  $error = false;
  $reason = array();
  if(! in_array($input["role"], array("customer","owner"))) {
    $error = $error || true;
    array_push($reason, "Role is invalid. Please choose either Customer or Restaurant owner");
  }
  if(! preg_match("/^[a-zA-Z][a-zA-Z0-9]{3,31}$/",$input["username"])) {
    $error = $error || true;
    array_push($reason, "Username is invalid. Please choose a username with length 4-32 that starts with a letter and only contains letters and digits.");
  }
  if(! preg_match("/^[a-zA-Z][a-zA-Z0-9]{5,31}$/",$input["password"])) {
    $error = $error || true;
    array_push($reason, "Password is invalid. Please choose a password with length 6-32 that only contains letters and digits and starts with an alphabetic character.");
  }
  if(strcmp($input["password"],$input["password2"]) != 0){
    $error = $error || true;
    array_push($reason, "Passwords does not match.");
  }
  if(! preg_match("/^(\s*)?([a-zA-Z]{3,}(\s*)?)+$/",$input["firstname"])) {
    $error = $error || true;
    array_push($reason, "First name is invalid. Please write your name without using any special characters.");
  }
  if(! preg_match("/^(\s*)?([a-zA-Z]{2,}(\s*)?)+$/",$input["lastname"])) {
    $error = $error || true;
    array_push($reason, "Last name is invalid. Please write your name without using any special characters.");
  }
  if(! preg_match("/^(\s*)?([\+\-0-9\s](\s*)?)+$/",$input["phone"])) {
    $error = $error || true;
    array_push($reason, "Phone is invalid. Please write your phone number using only digits, plus sign and dash.");
  }
  $input["error"] = $error;
  $input["reason"] = $reason;
  return $input;
}

function eatit_register_sanitize($input) {
  // TODO: implement sanitization
  return $input;
}

function eatit_login_form() {
  $response = eatit_login();
  if (isset($response["success"]) && $response["success"])
  redirect("index.php");
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="login-form a-form" action="login.php" method="post">
  <input type="text" name="username" value="<?php echo isset($response["username"]) ? $response["username"] : ""; ?>" placeholder="username">
  <input type="password" name="password" value="" placeholder="password">
  <input type="submit" name="login-submit" value="Login">
</form>

<?php
}

function eatit_login() {
  if (! isset($_POST["login-submit"])) {
    return array("error"=>false);
  } else {
    $raw = array(
      "username" => $_POST["username"],
      "password" => $_POST["password"],
    );
    $valid = eatit_login_validate($raw);
    if ($valid["error"]) {
      $valid["password"] = "";
      $valid["password2"] = "";
      return $valid;
    } else {
      $ready = eatit_login_sanitize($valid);
      $user = get_user_by("username",$ready["username"]);
      if (empty($user)) {
        $valid["error"] = true;
        $valid["reason"] = array("Incorrect username or password.");
        return $valid;
      }
      if (! is_correct_password($user["uid"],$ready["password"])) {
        $valid["error"] = true;
        $valid["reason"] = array("Incorrect username or password.");
        return $valid;
      }
      eatit_create_session_for($user);
      return array("error"=>false,"success"=>true, "uid"=>$uid);
    }
  }
}

function eatit_login_validate($input) {
  $error = false;
  $reason = array();
  if(! preg_match("/^[a-zA-Z][a-zA-Z0-9]{3,31}$/",$input["username"])) {
    $error = $error || true;
    array_push($reason, "Incorrect username or password.");
  }
  if(! preg_match("/^[a-zA-Z0-9]{6,32}$/",$input["password"])) {
    $error = $error || true;
    array_push($reason, "Incorrect username or password.");
  }
  $input["error"] = $error;
  $input["reason"] = $reason;
  return $input;
}

function eatit_login_sanitize($input) {
  // TODO: implement sanitization
  return $input;
}

function eatit_logout() {
  eatit_session_destroy();
}

function eatit_profile_edit_form() {
  $response = eatit_profile_update();
  if (isset($response["success"]) && $response["success"])
  echo "<p>Your profile is updated successfully.</p>";
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="profile-form a-form" action="profile.php" method="post">
  <input type="text" name="username" value="<?php echo isset($response["username"]) ? $response["username"] : ""; ?>" placeholder="username">
  <input type="password" name="password" value="" placeholder="password">
  <input type="password" name="password2" value="" placeholder="repeat password">
  <input type="text" name="firstname" value="<?php echo isset($response["firstname"]) ? $response["firstname"] : ""; ?>" placeholder="first name">
  <input type="text" name="lastname" value="<?php echo isset($response["lastname"]) ? $response["lastname"] : ""; ?>" placeholder="last name">
  <input type="text" name="phone" value="<?php echo isset($response["phone"]) ? $response["phone"] : ""; ?>" placeholder="phone number">
  <input type="submit" name="profile-submit" value="Update">
</form>
<?php
}

function eatit_profile_update() {
  $user = the_user();
  if (! isset($_POST["profile-submit"])) {
    return array(
      "username" => $user["username"],
      "password" => "",
      "password2" => "",
      "firstname" => $user["name"],
      "lastname" => $user["surname"],
      "phone" => $user["phone"],
      "error" => false,
    );
  } else {
    $raw = array(
      "username" => $_POST["username"],
      "password" => $_POST["password"],
      "password2" => $_POST["password2"],
      "firstname" => $_POST["firstname"],
      "lastname" => $_POST["lastname"],
      "phone" => $_POST["phone"],
    );
    $valid = eatit_profile_validate($raw);
    if ($valid["error"]) {
      return array(
        "username" => $user["username"],
        "password" => "",
        "password2" => "",
        "firstname" => $user["name"],
        "lastname" => $user["surname"],
        "phone" => $user["phone"],
        "error" => true,
        "reason" => $valid["reason"]
      );
    } else {
      $ready = eatit_register_sanitize($valid);
      // working on it....
      if (!empty(get_user_by("username",$ready["username"])) && get_user_by("username",$ready["username"])["uid"] != session_uid()) {
        $valid["reason"] = array("There already exists another registered user with the same username. Please choose a different username.");
        return array(
          "username" => $user["username"],
          "password" => "",
          "password2" => "",
          "firstname" => $user["name"],
          "lastname" => $user["surname"],
          "phone" => $user["phone"],
          "error" => true,
          "reason" => $valid["reason"]
        );
      } else {
        $changes = array(
          "username" => $ready["username"],
          "name" => $ready["firstname"],
          "surname" => $ready["lastname"],
          "phone" => $ready["phone"],
        );
        if ($ready["password"] != "") $changes["password"] = $ready["password"];
        $success = update_user(session_uid(),$changes);
        $user = the_user();
        return array(
          "username" => $user["username"],
          "password" => "",
          "password2" => "",
          "firstname" => $user["name"],
          "lastname" => $user["surname"],
          "phone" => $user["phone"],
          "error" => false,
          "success" => $success
        );
      }
    }
  }
}


function eatit_profile_validate($input) {
  $error = false;
  $reason = array();
  if(! preg_match("/^[a-zA-Z][a-zA-Z0-9]{3,31}$/",$input["username"])) {
    $error = $error || true;
    array_push($reason, "Username is invalid. Please choose a username with length 4-32 that starts with a letter and only contains letters and digits.");
  }
  if($input["password"] != "" && !preg_match("/^[a-zA-Z][a-zA-Z0-9]{5,31}$/",$input["password"])) {
    $error = $error || true;
    array_push($reason, "Password is invalid. Please choose a password with length 6-32 that only contains letters and digits and starts with an alphabetic character.");
  }
  if(strcmp($input["password"],$input["password2"]) != 0){
    $error = $error || true;
    array_push($reason, "Passwords does not match.");
  }
  if(! preg_match("/^(\s*)?([a-zA-Z]{3,}(\s*)?)+$/",$input["firstname"])) {
    $error = $error || true;
    array_push($reason, "First name is invalid. Please write your name without using any special characters.");
  }
  if(! preg_match("/^(\s*)?([a-zA-Z]{2,}(\s*)?)+$/",$input["lastname"])) {
    $error = $error || true;
    array_push($reason, "Last name is invalid. Please write your name without using any special characters.");
  }
  if(! preg_match("/^(\s*)?([\+\-0-9\s](\s*)?)+$/",$input["phone"])) {
    $error = $error || true;
    array_push($reason, "Phone is invalid. Please write your phone number using only digits, plus sign and dash.");
  }
  $input["error"] = $error;
  $input["reason"] = $reason;
  return $input;
}

function eatit_stringify_address($address) {
  return $address['street'].", ".$address['door_number'].". ".$address['district_name']."/".$address['city_name'];
}

function eatit_user_address_list() {
  ?>
  <ul class="address-list">
    <?php
    $aids = get_addresses_of_user(the_user()['uid']);
    if (! empty($aids)){
      echo "<li>";
      echo implode("</li><li>",array_map(function ($el) { return "<a href=find.php?district_code=".$el['district_code'].">".eatit_stringify_address($el)."</a>";},array_map("get_address_by_id",$aids)));
      echo "</li>";
    }
    ?>
  </ul>
  <?php
}

function eatit_new_address_form() {
  $response = eatit_new_address();
  if (isset($response["success"]) && $response["success"])
  redirect("index.php");
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="address-form a-form" action="index.php" method="post">
  <select class="city-select" name="city" onchange="setDistrict();">
    <option value="0" selected>--select city--</option>
    <?php array_map(function ($c) {echo "<option value='".$c['city_code']."'>".$c['city_code']." - ".$c['city_name']."</option>";},get_all_cities()); ?>
  </select>
  <select class="district-select" name="district">
    <option value="0" selected>--select district--</option>
    <?php array_map(function ($d) {echo "<option city='".$d['city_code']."' value='".$d['district_code']."'>".$d['district_name']."</option>";},get_all_districts()); ?>
  </select>
  <input type="number" name="door-number" value="<?php echo isset($response["door-number"]) ? $response["door-number"] : ""; ?>" placeholder="door number">
  <input type="text" name="street" value="" placeholder="street">
  <input type="submit" name="address-submit" value="Add">
</form>

<?php
}

function eatit_new_address() {
  if (! isset($_POST["address-submit"])) {
    return array("error"=>false);
  } else {
    $aid = create_address(session_uid(),$_POST["door-number"],$_POST["street"],$_POST["district"]);
    return array("error"=>false,"success"=>($aid ? true : false));
  }
}


function eatit_find_get_param() {
  $response = array();
  $district_code = isset($_GET["district_code"]) ? $_GET["district_code"] : 0;
  $response = get_district_by_code($district_code);
  return $response;
}

function eatit_stringify_restaurant($restaurant) {
  $str = "<h4><a href='restaurant.php?rid=".$restaurant['rid']."'>".$restaurant["name"]." (".(is_restaurant_open($restaurant['rid']) ? "Open" : "Closed").")</a></h4>";
  $str .= "<p>Avg. delivery time: ".($restaurant["delivery_time"] ? $restaurant["delivery_time"]." min." : "Unknown" )." | Min. cost: ".($restaurant["min_cost"] ? $restaurant["min_cost"]." TL" : "Unknown" )."</p>";
  return $str;
}

function eatit_restaurant_list_for_district($district_code) {
  $restaurants = get_restaurants_in_district($district_code);
  ?>
  <ul>
    <?php if (!$restaurants || empty($restaurants)): ?>
      <p>
        No restaurant is available at this district.
      </p>
    <?php else:
      echo "<li>".implode("</li><li>",array_map("eatit_stringify_restaurant",$restaurants))."</li>";
    endif; ?>
  </ul>
  <?php
}


function eatit_restaurant_get_param() {
  return isset($_GET["rid"]) ? $_GET["rid"] : 0;
}

function eatit_stringify_menu_items($menu_item) {
  $str = "<h4> ". $menu_item["name"].(is_promotion($menu_item['iid']) ? " (".implode(" + ",array_map(function ($k) {return $k['number']." x ".$k['name'];},get_promoted_items($menu_item['iid']))).")":"")."</h4>";

  $str .= "<p> Price: ".$menu_item["price"]." | Rank: ".eatit_stringify_ranking($menu_item["rank"] ? $menu_item["rank"] : "No ranking").($menu_item["is_available"]==1 ?
          (is_user_logged_in() ? "<form id='add_to_cart_form' method='post' action='restaurant.php?rid=".$_GET["rid"]."'>
          <input class='count' type='number' min='1' name='menu_item_count' value='1'>
           <input type='hidden' name = 'menu_item_id' value='" . $menu_item["iid"]. "'>
           <input name='add_to_cart_submit' type='submit' value='Add to cart'></form>" :"") : " | This item is not available")."</p>";
  return $str;
}

function eatit_menu_item_list_for_restaurant($rid,$type){

  $menu_items = array_filter(get_items_in_menu($rid),function($item) use ($type){return strcmp($item["item_type"], $type) == 0;});

  ?>
    <?php if (!$menu_items || empty($menu_items)): ?>
    <?php else:
      ?>
      <div class="box">

  <h3> <?php echo $type?></h3>
  <ul>
      <?php
      echo "<li>".implode("</li><li>",array_map("eatit_stringify_menu_items",$menu_items))."</li>"; ?>
  </ul>
</div>
<?php endif;
}

function eatit_get_favorite_items($rid){
  $menu_items = get_most_ordered_items($rid);
  ?>
    <?php if (!$menu_items || empty($menu_items)):
    echo "There is no favorite";
    else:
      ?>
      <div class="box">

  <h3> <?php echo "Most ordered items"?></h3>
  <ul>
      <?php
      echo "<li>".implode("</li><li>",array_map("eatit_stringify_menu_items",$menu_items))."</li>";
       ?>
     </ul>
   </div>
   <?php endif;
}

function eatit_stringify_feedback($feedback,$recent_order){
  $str = "<h4><a href='restaurant.php?rid=".$recent_order['rid']."'>".$recent_order["restaurant_name"]."</a></h4>";
  $str .= "<p>Order Date: ".$recent_order["order_date"]."</p>";
  $str .= "<p>Order: ".$recent_order["order_items"]."</p>";
  $str .= "<p>Total cost: ".$recent_order["total_cost"]."</p>";
  if ($feedback && !empty($feedback)){
    $str .= "<p>Comment: ".$feedback["comment"]."</p>";
    $str .= "<p>Rank: ".eatit_stringify_ranking($feedback["ranking"])."</p>";
  }
  else{
    if($recent_order["is_delivered"]){
    $str .= "<a href='feedback.php?oid=".$recent_order['oid']."'>Give Feedback</a>";
  }
  }
  return $str;
}
function eatit_recent_orders_form() {
  $user_id = the_user()['uid'];
  $orders = get_recent_orders($user_id);
  $feedbacks = array_map(function($e){$a = get_order_feedback($e['oid']); return (empty($a)) ? array() : $a[0];},$orders);
  ?>
  <ul class="order-list">
    <?php if (!$orders || empty($orders)): ?>
      <p>
        No recent orders!
      </p>
    <?php else:
      echo "<li>".implode("</li><li>",array_map("eatit_stringify_feedback",$feedbacks,$orders))."</li>";
    endif; ?>
  </ul>
  <?php
}

function eatit_feedback_form($oid) {
  $response = eatit_new_feedback($oid);
  if (isset($response["success"]) && $response["success"])
  redirect("index.php");
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="feedback-form a-form" action="feedback.php?oid=<?php echo $oid;?>" method="post">
  Comment:
</br>
  <textarea cols="20" rows="5" name="comment" value="" placeholder="Comment"></textarea>
  </br>
  Ranking:
  <input class='rankrange' type="range" name="rankingRange" min="1" max="10" value="1" oninput="this.form.rankingInput.value=this.value" >
  <input class='count' type="number" name="rankingInput" min="1" max="10" value="1" size="1" oninput="this.form.rankingRange.value=this.value" />
  <?php
   ?>
  <input type="submit" name="feedback-submit" value="Add">
</form>

<?php
}

function eatit_new_feedback($oid){
  if (! isset($_POST["feedback-submit"])) {
    return array("error"=>false);
  } else {
    $ans = add_order_feedback($oid,$_POST['comment'],$_POST['rankingInput']);
    var_dump($_POST['comment']);
    var_dump($_POST['ranking']);
    return array("error"=>false,"success"=>($ans ? true : false));

  }
}

function eatit_stringify_cart_item($item) {
  $str = "<form method='POST' action='".get_current_page().".php?".$_SERVER['QUERY_STRING']."'>";
  $str .= $item['count']." x ".$item['name']." (".number_format($item['count']*$item['price'],2).")";
  $str .= "<input type='hidden' name='iid' value='".$item['iid']."'>";
  $str .= "<input class='x-button' type='submit' name='remove-item' value='x'></form>";
  return $str;
}

function eatit_get_cart() {
  if(isset($_POST["remove-item"])) {
    remove_item_from_cart(session_uid(),$_POST["iid"]);
  }
  $cart = get_items_on_cart(session_uid());
  ?>
  <h3>Your cart</h3>
  <ul>
    <?php if (empty($cart)): ?>
      <p>
        You don't have any item in your cart.
      </p>
    <?php else: ?>
      <li><?php echo implode("</li><li>",array_map("eatit_stringify_cart_item",$cart)); ?></li>
      <span>Total: <?php echo number_format(array_reduce(array_map(function ($i) {return $i['count']*$i['price'];},$cart), function ($a,$b) {return $a+$b;}, 0),2); ?></span>
    <?php endif; ?>
  </ul>
  <?php
}

function eatit_owner_restaurants_list() {
  $rids = get_restaurants_of_user(session_uid());
  ?>
  <ul>
    <?php if (!$rids || empty($rids)): ?>
      <p>
        You have no restaurant.
      </p>
    <?php else: ?>
      <li><?php echo implode("</li><li>",array_map(
      function ($rid) {
        $r = get_restaurant_by("rid",$rid['rid']);
        return "<a href='restaurant.php?rid=".$r['rid']."'>".$r["name"]."</a>".(is_restaurant_open($rid['rid']) ? " (Open)" : " (Closed)");
      }, $rids)) ?></li>
    <?php endif; ?>
  </ul>
  <?php
}

function eatit_new_restaurant_form() {
  $response = eatit_new_restaurant();
  if (isset($response["success"]) && $response["success"])
  redirect("index.php");
  if ($response["error"]): ?>
  <ul class="error-list">
    <li><?php echo implode("</li><li>",$response["reason"]); ?></li>
  </ul>
<?php endif; ?>
<form class="restaurant-form a-form" action="index.php" method="post">
  <input type="text" name="name" value="" placeholder="name">
  <select class="address-select" name="address">
    <option value="0" selected>--select address--</option>
    <?php array_map(function ($d) {echo "<option value='".$d['aid']."'>".eatit_stringify_address(get_address_by_id($d['aid']))."</option>";},get_addresses_of_user(session_uid())); ?>
  </select>
  <input class="hour" length=5 type="text" name="open-hour" value="" placeholder="open hour"> -
  <input class="hour" length=5 type="text" name="close-hour" value="" placeholder="close hour"><br>
  <?php
  array_map(function ($p) {echo "<label><input type='checkbox' name='canpay[]' value='".$p["payment_type"]."'/>".$p['payment_type']."</label>";},get_all_payment_types());
   ?>
  <input type="submit" name="restaurant-submit" value="Add">
</form>

<?php
}

function eatit_new_restaurant() {
  if (! isset($_POST["restaurant-submit"])) {
    return array("error"=>false);
  } else {
    $rid = create_restaurant($_POST["address"],$_POST["name"],$_POST["open-hour"],$_POST["close-hour"]);
    if (! $rid) return array("error"=>true,"reason"=>array("Could not add new restaurant"));
    if (isset($_POST['canpay'])) array_map(function ($p) use ($rid) { add_payment_option($rid,$p); },$_POST['canpay']);
    return array("error"=>false,"success"=>($rid ? true : false));
  }

}

function eatit_stringify_owner_order($feedback,$recent_order) {
  $orderer = get_user_by("uid",get_address_by_id("aid",$recent_order["aid"])["uid"]);
  $str = "<p>By ".$orderer['name']." ".$orderer["surname"]."<p>";
  $str .= "<p>To: <a href='restaurant.php?rid=".$recent_order['rid']."'>".get_restaurant_by("rid",$recent_order['rid'])["name"]."</a></p>";
  $str .= "<p>Order Date: ".$recent_order["order_date"]."</p>";
  $str .= "<p>Delivery Date: ".$recent_order["delivery_date"]."</p>";
  $str .= "<p>Order: ".implode(", ",array_map(function ($i) {return $i["name"].(is_promotion($i['iid']) ? " (".implode(" + ",array_map(function ($k) {return $k['number']." x ".$k['name'];},get_promoted_items($i['iid']))).")":"")." x ".$i['count'];},get_items_in_order($recent_order["oid"])))."</p>";
  $str .= "<p>Total cost: ".$recent_order["total_cost"]."</p>";
  $str .= "<p>Address: ".eatit_stringify_address(get_address_by_id("aid",$recent_order["aid"]))."</p>";
  if ($feedback && !empty($feedback)){
    $str .= "<p>Comment: ".$feedback["comment"]."</p>";
    $str .= "<p>Rank: ".eatit_stringify_ranking($feedback["ranking"])."</p>";
  }
  $str .= "<p>Status: ".($recent_order["is_delivered"] ? "Delivered" : "Processing")."</p>";
  if (! $recent_order['is_delivered']) {
    $str .= "<form class='deliver-form a-form' method='post' action='index.php'>";
    $str .= "<input type='hidden' name='oid' value='".$recent_order['oid']."'>";
    $str .= "<input type='submit' name='deliver-submit' value='Set delivered'>";
    $str .= "</form>";
  }
  return $str;
}

function eatit_owner_orders_list() {
  if (isset($_POST["deliver-submit"])) {
    set_order_delivered($_POST["oid"],true);
    redirect("index.php");
  }
  $orders = get_orders_for_owner(session_uid());
  $feedbacks = array_map(function($e){$a = get_order_feedback($e['oid']); return (empty($a)) ? array() : $a[0];},$orders);
  if(empty($orders)) {
    ?> <p>
      You have no order around here.
    </p> <?php
  } else {
    ?>
    <ul class="order-list">
      <li>
        <?php echo implode("</li><li>",array_map("eatit_stringify_owner_order",$feedbacks,$orders)); ?>
      </li>
    </ul>
    <?php
  }
}

function eatit_stringify_ranking($rank) {
  if ($rank == "No ranking") {
    return "<span class='rank no-rank'>$rank</span>";
  } else {
    return "<span class='rank rank-".round($rank/2)."'>$rank</span>";
  }
}
