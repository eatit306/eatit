<?php

function db_query($querystr) {
  $result = array();
  $result['error'] = false;

  // connect using global variables
  global $db_host, $db_user, $db_pass, $db_name;
  $con  = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

  if (mysqli_connect_errno()) {
    $result['error'] = true;
    $result['reason'] = "MySQL connection error: ".mysqli_connect_error()." (".mysqli_connect_errno().")";
  } else {

    //mysqli_query($con,"SET NAMES utf8");

    $response = mysqli_query($con,$querystr);
    if (!$response) {
      $result['error'] = true;
      $result['reason'] = "MySQL error: ".mysqli_error($con);
    } else {
      $count = mysqli_affected_rows($con);
      $result['count'] = $count;
      $result['id'] = mysqli_insert_id($con);
      $result['rows'] = array();

      if (gettype($response) != "boolean"){

        while($row = mysqli_fetch_array($response)) {
          array_push($result['rows'],$row);
        }
      }
    }
  }

  // close connection!
  mysqli_close($con);
  return $result;
}

function db_select($from,$columns=array("*"),$condition=array("1"),$by="") {
  return db_query("SELECT ".implode($columns,",")." FROM ".implode($from,",")." WHERE ".implode($condition," AND ")." ".$by);
}

function db_insert($into,$vals,$cols=false,$multiple=false) {
  return db_query("INSERT INTO $into".($cols ? " (".implode($cols,",").")" : "")." VALUES ".implode(array_map(function($el){return "(".implode(array_map("db_process_single_input",$el),",").")";},$multiple ? $vals : array($vals)),","));
}

function db_delete($from,$condition) {
  return db_query("DELETE FROM $from WHERE ".implode($condition, " AND "));
}

function db_update($table,$changes,$condition) {
  return db_query("UPDATE $table SET ".implode(array_map_assoc(function($k,$v){return "$k = $v";},array_map("db_process_single_input",$changes)),",")." WHERE ".implode($condition, " AND "));
}

function db_process_single_input($val) {
  switch(gettype($val)) {
    case "string":
    return "'$val'";
    case "boolean":
    return $val ? "1" : "0";
    default:
    return $val;
  }
}
