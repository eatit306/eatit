<?php

global $_SESSION;

function eatit_session_init() {
  session_start();
  if (! isset($_SESSION['eatit']))
    $_SESSION['eatit'] = array();
}

function eatit_session_destroy() {
  unset($_SESSION['eatit']);
  session_destroy();
}

function eatit_create_session_for($user) {
  $_SESSION['eatit']['logged_in'] = true;
  $_SESSION['eatit']['uid'] = $user['uid'];
}

function is_user_logged_in() {
  return isset($_SESSION['eatit']["logged_in"]) && $_SESSION['eatit']["logged_in"];
}

function session_uid() {
  return isset($_SESSION['eatit']['uid']) ? $_SESSION['eatit']['uid'] : -1;
}

function get_current_page() {
  return explode(".", basename($_SERVER['PHP_SELF']))[0];
}

function eatit_sorting_hat() {
  if (is_user_logged_in()) {
    switch (get_current_page()) {
      case 'register':
      case 'login':
        header("Location: index.php");
        return;
    }
  } else {
    switch (get_current_page()) {
      case 'order':
      case 'feedback':
      case 'logout':
        header("Location: index.php");
        return;
    }
  }
}
