<?php
require 'init.php';
eatit_header();
?>

<h2> <?php echo eatit_get_title();?> </h2>



<?php if (is_user_logged_in() && is_owner(session_uid()) && in_array($_GET["rid"],array_map(function ($r) {return $r['rid'];},get_restaurants_of_user(session_uid())))): ?>
  <div class="is-open box">
      <?php
      if(isset($_POST['io'])) {
          set_restaurant_open($_GET['rid'],$_POST["io"] == "Set open");
          redirect("restaurant.php?rid=".$_GET["rid"]);
      }
       ?>
       Restaurant is currently <?php echo (is_restaurant_open($_GET['rid'])) ? "open." : "closed."; ?>
       <form class="io-restaurant" action="restaurant.php?rid=<?php echo $_GET['rid']; ?>" method="post">
        <input type="submit" name="io" value="Set <?php echo (get_restaurant_by("rid",$_GET['rid'])["is_open"]) ? "closed": "open"; ?>">
      </form>

  </div>
  <div class="serving-districts box">
    <h4>This restaurant currently serves</h4>
    <?php
    if(isset($_POST['remove-district'])) {
        $ans = remove_service_district($_GET["rid"],$_POST["district"]);
        redirect("restaurant.php?rid=".$_GET["rid"]);
    }
    $districts = get_service_districts($_GET['rid']);
    if (!empty($districts)) {
      echo implode(", ",array_map(function ($r) {return $r['district_name']
        ."<form class='district-remove-form a-form' method='POST' action='restaurant.php?rid=".$_GET['rid']."'><input type='hidden' name='district' value='".$r["district_code"]."'><input class='x-button' type='submit' name='remove-district' value='x'></form>";},$districts));
    } else {
      echo "Everywhere!";
    }
    ?>
  </div>
  <div class="add-district box">
    <h4>Add service distict</h4>
    <?php
    if(isset($_POST['district-submit'])) {
        $ans = add_service_district($_GET["rid"],$_POST["district"],$_POST["delivery-time"],$_POST["min-cost"]);
        redirect("restaurant.php?rid=".$_GET["rid"]);
    }
     ?>
    <form class="service-form a-form" action="restaurant.php?rid=<?php echo $_GET['rid']; ?>" method="post">
      <select class="city-select" name="city" onchange="setDistrict();">
        <option value="0" selected>--select city--</option>
        <?php array_map(function ($c) {echo "<option value='".$c['city_code']."'>".$c['city_code']." - ".$c['city_name']."</option>";},get_all_cities()); ?>
      </select>
      <select class="district-select" name="district">
        <option value="0" selected>--select district--</option>
        <?php array_map(function ($d) {echo "<option city='".$d['city_code']."' value='".$d['district_code']."'>".$d['district_name']."</option>";},get_all_districts()); ?>
      </select><br>
      <label>Minimum cost for delivery: <input type="number" step="0.05" min="0" name="min-cost" value="0" placeholder="minimum cost"> TL</label><br>
      <label>Average delivery time: <input type="number" min="1" max="120" name="delivery-time" value="60" placeholder="average delivery time"> minutes</label>
      <input type="submit" name="district-submit" value="Add">
    </form>
  </div>
  <div class="add-menu-item box">
    <h4>Add menu item</h4>
    <?php
    if(isset($_POST['menu-item-submit'])) {
        $iid = create_menu_item($_POST['name'],$_POST['price'],$_POST['type']);
        add_item_to_menu($iid, $_GET['rid']);
        redirect("restaurant.php?rid=".$_GET["rid"]);
    }
     ?>
    <form class="menu-item-form a-form" action="restaurant.php?rid=<?php echo $_GET['rid']; ?>" method="post">
      <input type="text" name="name" value="" placeholder="name">
      <input class="price" type="number" min="0" step="0.05" name="price" value="" placeholder="price">
      <select class="type-select" name="type">
        <option value="0" selected>--select type--</option>
        <?php array_map(function ($d) {echo "<option value='".$d['item_type']."'>".$d['item_type']."</option>";},get_all_item_types()); ?>
      </select>
      <input type="submit" name="menu-item-submit" value="Add">
    </form>
  </div>
  <div class="add-promotion box">
    <h4>Add promotion</h4>
    <?php
    if(isset($_POST['promotion-submit'])) {
        $iid = create_menu_item($_POST['name'],$_POST['price'],"Promotion");
        add_item_to_menu($iid, $_GET['rid']);
        $items = array($_POST["item-1"],$_POST["item-2"],$_POST["item-3"],$_POST["item-4"],$_POST["item-5"]);
        $numbers = array($_POST["number-1"],$_POST["number-2"],$_POST["number-3"],$_POST["number-4"],$_POST["number-5"]);
        array_map(function ($i,$n) use ($iid) { if ($i > 0) add_to_promotion($iid,$i,$n); },$items,$numbers);
        redirect("restaurant.php?rid=".$_GET["rid"]);
    }
     ?>
    <form class="promotion-form a-form" action="restaurant.php?rid=<?php echo $_GET['rid']; ?>" method="post">
      <input type="text" name="name" value="" placeholder="name">
      <?php for ($i=1; $i <= 5; $i++) { ?>
        <div class="pitem">

      <select class="promotion-item-select" class="item-select" name="item-<?php echo $i ?>">
        <option value="0" selected>--select item--</option>
        <?php array_map(function ($d) {echo "<option value='".$d['iid']."'>".$d['name']."</option>";},get_items_in_menu($_GET["rid"])); ?>
      </select>
      <input class="count" type="number" min="1" name="number-<?php echo $i ?>" value="1">
    </div>
      <?php } ?>
      <input class="price" type="number" min="0" step="0.05" name="price" value="" placeholder="price">
      <input type="submit" name="promotion-submit" value="Add">
    </form>
  </div>
  <div class="menu-items box">
    <h4>Items</h4>

      <?php
      if(isset($_POST['available-submit'])) {
          set_menu_item_available($_POST["iid"],!is_menu_item_available($_POST['iid']));
          redirect("restaurant.php?rid=".$_GET["rid"]);
      }
      $items = get_items_in_menu($_GET["rid"]);
      ?>
      <ul>
        <?php
        if (!empty($items))
        echo "<li>".implode("</li><li>", array_map(function ($i){ return $i["name"].(is_promotion($i['iid']) ? " (".implode(" + ",array_map(function ($k) {return $k['number']." x ".$k['name'];},get_promoted_items($i['iid']))).")":"").", ".$i["price"]."<form class='available-form a-form' method='POST' action='restaurant.php?rid=".$_GET['rid']."'><input type='hidden' name='iid' value='".$i['iid']."'><input class='x-button ".($i['is_available'] ? "green":"red")."' type='submit' name='available-submit' value='".($i['is_available'] ? "-":"+")."'></form>"; }, $items))."</li>";
        ?>
      </ul>
  </div>
<?php else: ?>
<?php
    $rid = eatit_restaurant_get_param();
    if (isset($_POST["add_to_cart_submit"])) {
      add_item_to_cart(session_uid(),$_POST["menu_item_id"],$rid,$_POST["menu_item_count"]);
      redirect("restaurant.php?rid=".$_GET["rid"]);
    }
?>

<?php

if(is_restaurant_open($rid)){
    eatit_get_favorite_items($rid);
    eatit_menu_item_list_for_restaurant($rid,"Promotion");
    eatit_menu_item_list_for_restaurant($rid,"Main Dish");
    eatit_menu_item_list_for_restaurant($rid,"Appetizer");
    eatit_menu_item_list_for_restaurant($rid,"Dessert");
    eatit_menu_item_list_for_restaurant($rid,"Drink");
} else {
    echo "This restaurant cannot serve at the moment.";
}?>
<?php endif; ?>
<?php
eatit_footer();
