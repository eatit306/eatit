<?php
include 'init.php';
eatit_header();
?>
<?php if (is_user_logged_in()): ?>
  <?php if (is_customer(session_uid())) : ?>
  <div class="addresses box">
    <h3>Your addresses</h3>
    <?php eatit_user_address_list(); ?>
  </div>
  <div class="add-address box">
    <h3>Add new adress</h3>
    <?php eatit_new_address_form(); ?>
  </div>
  <div class="prev-orders box">
    <h3>Your most recent orders</h3>
    <?php eatit_recent_orders_form(); ?>
  </div>
<?php endif; if (is_owner(session_uid())) : ?>
  <div class="restaurants box">
    <h3>Your restaurants</h3>
    <?php eatit_owner_restaurants_list(); ?>
  </div>
  <div class="add-address box">
    <h3>Add new adress</h3>
    <?php eatit_new_address_form(); ?>
  </div>
  <div class="add-restaurant box">
    <h3>Add new restaurant</h3>
    <?php eatit_new_restaurant_form(); ?>
  </div>
  <div class="next-orders box">
    <h3>Orders</h3>
    <?php eatit_owner_orders_list(); ?>
  </div>
  <?php endif; ?>

<?php endif;   ?>

<?php
eatit_footer();
