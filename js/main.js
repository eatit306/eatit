var all_districts;
$(document).ready(function() {
  all_districts = $(".district-select").html();
  $(".district-select").attr("disabled",true);
});

function setDistrict() {
  $(".district-select").attr("disabled",true);
  $(".district-select").html(all_districts);
  city_code = $(".city-select option:selected").val();
  $(".district-select option").each(function() {
    if($(this).attr("city") !== undefined) {
      if ($(this).attr("city") != city_code) {
        $(this).remove();
      }
    }
  });
  $(".district-select").removeAttr("disabled");
}
