# EatIT - An attempt for a food ordering site #

*Atılberk Çelebi, Ceyhun Onur, and Umur Şen*

### What is this? ###

As the final project of Database Management Systems course taught by Prof. Attila Gursoy from Koç University Computer Engineering Department, in order to gain experience on design and implementation of relational databases, we have designed the database architecture of EatIT, a web application similar to YemekSepeti.com, and later implemented it using MySQL and Vanilla PHP.

However, this project is not yet complete and fully functional because it has started as a simple course project and needed to be delivered in limited time with a little constraints. We opened the repository to exhibit our approach and effort with the hope of that it might be helpful to someone as an example (bad or good).

This much helped us to get the full point, hope it helps you to get started for your own project!

### How do I get set up? ###

* Clone the repository into your apache servers `www` directory
* Create a database in your MySQL and run `sqls/ddl.sql` to create tables. Optionally, you can run to populate your database with sample content using `sqls/pop.sql`
* Set the `include/globals.php`